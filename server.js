var io = require('socket.io').listen(3000);
var clients = [];
io.set('log level',0);
io.sockets.on('connection',function(socket){
  console.log('Someone has connection');
  socket.on('register',function(id,name,callback){
    console.log('Register',id,name);
    var user = { 
      'name':name,
      'id':id,
      'socket':socket
    };

    socket.user_info = user;
    user = socket;
    clients.push(user);


    var array_user = [];
    for (var i = clients.length - 1; i >= 0; i--) {
      array_user.push({'name':clients[i].user_info.name ,'id':clients[i].user_info.id });
    };
    console.log(array_user);
    io.sockets.emit('user_update',array_user);


    if(callback){
      var meta = { 'user':{'name':name,'id':id}};
      var result = { 
        'code':200 , 
        'message':'ok',
        'meta': meta

      };

      callback(result);
    }
    // console.log(clients);
  });
  socket.on('broadcast',function(message,callback){
    var data = {
      'message':message,
      'user':socket.user_info.name
    }

    console.log(message);
    
    io.sockets.emit('broadcast',data);


    if(callback){
      var meta = { 'user':{'name':socket.user_info.name,'id':socket.user_info.id,'message':message}};
      var result = { 
        'code':200 , 
        'message':'ok',
        'meta': meta

      };

      callback(result);
    }
  });
  socket.on('pm',function(message,user_id){
    console.log(user_id);
    for (var i = 0; i < clients.length; i++) {
      if(clients[i].user_info.id == user_id){
        clients[i].emit('pm',message,socket.user_info.name);        
      }
    };

  });
  socket.on('disconnect',function(){
    for (var i = 0; i < clients.length; i++) {
      if(clients[i].user_info.id == socket.user_info.id){
        clients.splice(i,1);
      }
    };

    var array_user = [];
    for (var i = clients.length - 1; i >= 0; i--) {
      array_user.push({'name':clients[i].user_info.name ,'id':clients[i].user_info.id });
    };
    console.log(array_user);
    io.sockets.emit('user_update',array_user);
    if(socket.user_info){
      var data = {
        'message': 'has left room',
        'user': socket.user_info.name
      };
      console.log(data);
      io.sockets.emit('broadcast', data);
      
    }
  })

});